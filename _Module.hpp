#include "../DModuleCore.hpp"
#include "dref/DModule.hpp"
#include "dwrapper_googletest/DModule.hpp"

#if DMACRO_CheckModuleVersion(dref, 1, 1) &&\
    DMACRO_CheckModuleVersion(dwrapper_googletest, 1, 1)
#define DModule_dref_tests_Version 1
#endif
