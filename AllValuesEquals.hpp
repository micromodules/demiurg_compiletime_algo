#pragma once

#include <initializer_list>

template<typename T_Type>
bool areAllValuesEqualsTo(const T_Type inValue, const std::initializer_list<T_Type>& inValues) {
    for (const T_Type& theValue : inValues)
        if (theValue != inValue)
            return false;
    return true;
}

template<typename T_Type>
bool areAllValuesEquals(const std::initializer_list<T_Type>& inValues) {
    return areAllValuesEqualsTo(inValues[0], inValues);
}

namespace DConstexp
{
    template<typename T_Type>
    constexpr bool areAllValuesEqualsTo(const T_Type inValue, const std::initializer_list<T_Type>& inValues) {
        for (const T_Type& theValue : inValues)
            if (theValue != inValue)
                return false;
        return true;
    }

    template<typename T_Type>
    constexpr bool areAllValuesEquals(const std::initializer_list<T_Type>& inValues) {
        return constAreAllValuesEqualsTo(*inValues.begin(), inValues);
    }
}//namespace DConstexp
